import org.apache.spark.{SparkContext, SparkConf}
import util.GlobalArgs

/**
  * Created by chenjianwen on 2016/3/2.
  */
object CFContext {
    val conf = new SparkConf().setMaster(GlobalArgs.master_url).setAppName("YCF_CF");
    val sc =  new SparkContext(conf)

}
